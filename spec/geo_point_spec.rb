require 'test/unit'
require './lib/geo_point'

class GeoPointSpec < Test::Unit::TestCase
  def test_radians_conversion
    point = GeoPoint.new(0, 0)
    assert_equal Math::PI / 12, point.to_rad(15)
  end

  def test_angles_converted_to_radians
    center = GeoPoint.new(60, 30)
    assert_equal Math::PI / 3, center.latitude_rad
    assert_equal Math::PI / 6, center.longitude_rad
  end

  def test_stringified_angles_converted_to_floats
    center = GeoPoint.new("60.345", "30.432")
    assert_equal 60.345, center.latitude
    assert_equal 30.432, center.longitude
  end

  def test_invalid_lat_string_value_raises_exception
    assert_raise ArgumentError do
      GeoPoint.new('60s', 30)
    end
  end

  def test_invalid_long_string_value_raises_exception
    assert_raise ArgumentError do
      GeoPoint.new(60, '30a')
    end
  end

  def test_zero_distance
    point = GeoPoint.new(0,0)
    assert_equal 0, GeoPoint.new(0,0).distance(point)
  end

  def test_zero_distance_with_same_point
    point = GeoPoint.new(59.9, 30)
    assert_equal 0, point.distance(point)
  end

  def test_large_distance
    dublin_coords = GeoPoint.new(53.3244427, -6.3861304)
    st_petersburg_coords = GeoPoint.new(59.9174911, 30.0441954)
    assert_equal 2_311_870, dublin_coords.distance(st_petersburg_coords)
  end

  def test_short_distance
    dublin_coords = GeoPoint.new(53.3244427, -6.3861304)
    belfast_coords = GeoPoint.new(54.5950191, -5.9968436)
    assert_equal 143_558, dublin_coords.distance(belfast_coords)
  end

  def test_shorter_distance
    manhattan_coords = GeoPoint.new(40.7591703, -74.0394428)
    brooklyn_coords = GeoPoint.new(40.6454195, -74.0854256)
    assert_equal 13_229, manhattan_coords.distance(brooklyn_coords)
  end

  def test_tiny_distance
    point_1 = GeoPoint.new(51.49871493, -0.1601177991)
    point_2 = GeoPoint.new(51.49840586, -0.1604068824)
    assert_equal 40, point_1.distance(point_2)
  end

  def test_distance_both_ways
    dublin_coords = GeoPoint.new(53.3244427, -6.3861304)
    belfast_coords = GeoPoint.new(54.5950191, -5.9968436)
    assert_equal 143_558, belfast_coords.distance(dublin_coords)
    assert_equal 143_558, dublin_coords.distance(belfast_coords)
  end

  def test_distance_rounding_error
    point_1 = GeoPoint.new(51.5188885, -0.1583307104)
    point_2 = GeoPoint.new(51.5188885, -0.1583307104)

    assert_nothing_raised Math::DomainError do
      point_1.distance(point_2)
    end
  end
end
