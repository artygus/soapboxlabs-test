require './lib/way_point'
require 'stringio'

class RouteParser
  class InvalidWaypoint < StandardError; end

  class << self
    def point_from_string(str)
      parts = str.strip.split(',')
      WayPoint.new(*parts)
    rescue WayPoint::NegativeTimestamp, ArgumentError => e
      raise InvalidWaypoint.new("String '#{str.strip}' is invalid, error raised: #{e.inspect}")
    end

    def read_from_stream(stream)
      stream.each_line.reduce([]) do |res, line|
        res << point_from_string(line)
      end
    end

    def from_file(path)
      File.open(path, 'r') do |f|
        read_from_stream(f)
      end
    end

    def point_to_string(way_point)
      [way_point.geo.latitude, way_point.geo.longitude, way_point.timestamp].join(',')
    end
  end

  private_class_method :read_from_stream
end
