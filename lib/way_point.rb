require './lib/geo_point'

class WayPoint
  class NegativeTimestamp < StandardError; end

  attr_reader :geo, :timestamp

  def initialize(latitude, longitude, timestamp)
    @timestamp = Integer(timestamp)
    raise NegativeTimestamp if @timestamp < 0
    @geo = GeoPoint.new(latitude, longitude)
  end

  def ==(other)
    other.is_a?(WayPoint) && geo == other.geo && timestamp == other.timestamp
  end
end
