require 'test/unit'
require './lib/gpx_exporter'
require './lib/way_point'

class GpxExporterSpec < Test::Unit::TestCase
  def test_serialize_way_point
    way_point = WayPoint.new(60.123, 30.345, 1)
    expected_xml = '<trkpt lat="60.123" lon="30.345">'\
                   '<time>1970-01-01T00:00:01Z</time></trkpt>'
    assert_equal expected_xml, GpxExporter.serialize_way_point(way_point)
  end

  def test_serialize
    way_points = [WayPoint.new(59.934, 30.335, 1),
                  WayPoint.new(53.35, -6.266, 2)]
    expected_xml = '<?xml version="1.0" encoding="UTF-8"?>'\
                   '<gpx version="1.1"><trk><trkseg>'\
                   '<trkpt lat="59.934" lon="30.335">'\
                   '<time>1970-01-01T00:00:01Z</time></trkpt>'\
                   '<trkpt lat="53.35" lon="-6.266">'\
                   '<time>1970-01-01T00:00:02Z</time></trkpt>'\
                   '</trkseg></trk></gpx>'
    assert_equal expected_xml, GpxExporter.serialize(way_points)
  end

  def test_export
    way_points = [WayPoint.new(59.934, 30.335, 1),
                  WayPoint.new(53.35, -6.266, 2)]
    expected_xml = '<?xml version="1.0" encoding="UTF-8"?>'\
                   '<gpx version="1.1"><trk><trkseg>'\
                   '<trkpt lat="59.934" lon="30.335">'\
                   '<time>1970-01-01T00:00:01Z</time></trkpt>'\
                   '<trkpt lat="53.35" lon="-6.266">'\
                   '<time>1970-01-01T00:00:02Z</time></trkpt>'\
                   '</trkseg></trk></gpx>'

    Tempfile.open('out.gpx') do |file|
      GpxExporter.export(file.path, way_points)
      assert_equal expected_xml, file.read
    end
  end
end
