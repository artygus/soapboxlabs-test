require 'test/unit'
require 'tempfile'
require './lib/route_parser'
require './lib/geo_point'

class RouteParserSpec < Test::Unit::TestCase
  def test_point_from_string
    line = '51.49871493,-0.1601177991,1326378718'
    point = RouteParser.point_from_string(line)
    assert_equal point, WayPoint.new(51.49871493, -0.1601177991, 1326378718)
  end

  def test_point_from_string_with_too_many_columns
    assert_raise RouteParser::InvalidWaypoint do
      RouteParser.point_from_string('51.49871493,-0.1601177991,1326378718,123')
    end
  end

  def test_point_from_string_with_too_few_columns
    assert_raise RouteParser::InvalidWaypoint do
      RouteParser.point_from_string('51.49871493,-0.1601177991,')
    end
  end

  def test_point_from_string_with_invalid_data
    assert_raise RouteParser::InvalidWaypoint do
      RouteParser.point_from_string('51.49871493,-0.1601177991,-1')
    end
  end

  def test_from_file
    points = [WayPoint.new(51.49871493, -0.1601177991, 1326378718), WayPoint.new(51.49840586, -0.1604068824, 1326378723)]
    parsed_points = RouteParser.from_file("./spec/fixtures/two_points.csv")
    assert_equal parsed_points, points
  end

  def test_from_file_empty
    parsed_points = RouteParser.from_file("./spec/fixtures/empty.csv")
    assert_equal parsed_points, []
  end

  def test_point_to_string
    point = WayPoint.new(51.49871493, -0.1601177991, 1326378718)
    assert_equal '51.49871493,-0.1601177991,1326378718', RouteParser.point_to_string(point)
  end
end
