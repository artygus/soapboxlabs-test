class GeoPoint
  EARTH_RADIUS = 6_371_000

  attr_reader :latitude, :longitude, :latitude_rad, :longitude_rad

  def initialize(latitude, longitude)
    @latitude = Float(latitude)
    @longitude = Float(longitude)

    @latitude_rad = to_rad(@latitude)
    @longitude_rad = to_rad(@longitude)
  end

  def to_rad(angle)
    angle.to_f / 180 * Math::PI
  end

  def distance(geo_point)
    central_angle = Math.sin(latitude_rad) * Math.sin(geo_point.latitude_rad) +
                    Math.cos(latitude_rad) * Math.cos(geo_point.latitude_rad) *
                    Math.cos((longitude_rad - geo_point.longitude_rad).abs)

    clamped_central_angle = [-1, central_angle, 1].sort[1]
    (Math.acos(clamped_central_angle) * EARTH_RADIUS).round
  end

  def ==(other)
    other.is_a?(GeoPoint) && other.latitude == latitude && other.longitude == longitude
  end
end
