require './lib/way_point'

class RouteVerifier
  class << self
    def measure_speed(way_point_1, way_point_2)
      distance = way_point_1.geo.distance(way_point_2.geo)
      timediff = (way_point_1.timestamp - way_point_2.timestamp).abs
      timediff.zero? ? 0 : distance.to_f / timediff
    end

    def check(way_points, max_reasonable_speed)
      i, prev_i = 1, 0
      erroneous_indexes = []

      while(i < way_points.size) do
        local_speed = measure_speed(way_points[i], way_points[prev_i])

        if local_speed <= max_reasonable_speed
          prev_i = i
        else
          erroneous_indexes << i
        end

        i += 1
      end

      erroneous_indexes
    end
  end
end
