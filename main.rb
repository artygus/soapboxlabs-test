#!/usr/bin/env ruby

require './lib/route_parser.rb'
require './lib/route_verifier.rb'

if ARGV[0].nil?
  puts "Route file is required, check out points.csv"
  exit 1
elsif !File.exists?(ARGV[0])
  puts "Can't locate file #{ARGV[0]}"
  exit 1
end

begin
  way_points = RouteParser.from_file(ARGV[0])
rescue RouteParser::InvalidWaypoint => e
  puts e
  exit 1
end

# 25 m/s = 90 km/h seem like the max reasonable speed allowed in city
erroneous_points = RouteVerifier.check(way_points, 25)

if erroneous_points.size > 0
  puts "The following rows seem to be incorrect"
  erroneous_points.each do |index|
    way_point = way_points[index]
    puts "  \##{index + 1} <#{RouteParser.point_to_string(way_point)}>"
  end
else
  puts 'Congrats! The route trace seems correct'
end
