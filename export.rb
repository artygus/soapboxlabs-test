#!/usr/bin/env ruby

require './lib/route_parser.rb'
require './lib/route_verifier.rb'
require './lib/gpx_exporter.rb'

if ARGV[0].nil?
  puts "Route file is required, check out points.csv"
  exit 1
elsif !File.exists?(ARGV[0])
  puts "Can't locate file #{ARGV[0]}"
  exit 1
end

begin
  way_points = RouteParser.from_file(ARGV[0])
rescue RouteParser::InvalidWaypoint => e
  puts e
  exit 1
end

# 25 m/s = 90 km/h seem like the max reasonable speed allowed in city
erroneous_points = RouteVerifier.check(way_points, 25)

ext_name = File.extname(ARGV[0])
base_name = File.basename(ARGV[0], ext_name)

file_path_original = "./#{base_name}.gpx"
file_path_cleaned = "./#{base_name}_cleaned.gpx"

GpxExporter.export(file_path_original, way_points)

cleaned_way_points = way_points.reject.each_with_index do |item, index|
  erroneous_points.include?(index)
end

GpxExporter.export(file_path_cleaned, cleaned_way_points)
