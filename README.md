Soapbox Labs Code Test
======================

## Tests

The test tasks implented in ruby, to run spec you would need ruby >= 2.0 and rake gem

    gem install rake
    cd <soapbox_code_test>
    rake test


## RouteVerifier

File `./main.rb` is an executable and requires file path as one single argument. I included points.csv for simplicity

    ./main.rb points.csv


## Export to GPX

In order to assure myself the solution works I needed a way to visualize results. I stumbled upon [My GPS Files](http://www.mygpsfiles.com/app/) service which allows to upload .gpx files and shows the route on map, it also allows to go through each point on map and see local characteristics like distance, speed, etc.

    ./export.rb points.csv

Produces `points.gpx` and `points_cleaned.gpx` files corresponding to original and cleaned routes.


## Notes

The program doesn't cover all possible cases as it's a test program and intended to be used solely as a demo. The following defects are not covered intentionally:

* program assumes waypoints are sorted by timestamp in order to make things easier
* no type checking for collections, ruby is a dynamic language and doesn't support this type of things out of the box
* GeoPoint#distance method uses the simplest formula for calculating great-circle distance and clamps central-angle to [1,-1] range to compensate imperfect precisicion of floating-point numbers, better approach would be to use harvestine or Vincenty formulas
* algorithm for finding erroneous way points is failry dumb and based on local speed between two points only and requires maximum reasonable speed assumption, better approach would be to solve it as classification problem for provided data set, using say gradient descent
* source row checking could be better, e.g. skipping rows with incorrect data; program throws error with row text instead
* no tests for `main.rb` and `export.rb` executables
