require 'test/unit'
require './lib/route_verifier'
require './lib/route_parser'

class RouteVerifierSpec < Test::Unit::TestCase
  def test_measure_speed
    way_point_1 = WayPoint.new(51.49871493, -0.1601177991, 1326378718)
    way_point_2 = WayPoint.new(51.49840586, -0.1604068824, 1326378723)
    assert_equal 8.0, RouteVerifier.measure_speed(way_point_1, way_point_2)
  end

  def test_measure_speed_same_way_point
    way_point = WayPoint.new(51.49871493, -0.1601177991, 1326378718)
    assert_equal 0.0, RouteVerifier.measure_speed(way_point, way_point)
  end

  def test_measure_speed_always_positive
    way_point_1 = WayPoint.new(51.49871493, -0.1601177991, 1326378718)
    way_point_2 = WayPoint.new(51.49840586, -0.1604068824, 1326378723)
    assert_equal RouteVerifier.measure_speed(way_point_2, way_point_1),
                 RouteVerifier.measure_speed(way_point_1, way_point_2)
  end

  def test_check_empty
    assert_equal [], RouteVerifier.check([], 0)
  end

  def test_check_empty_with_one_way_point
    way_point = WayPoint.new(51.49871493, -0.1601177991, 1326378718)
    assert_equal [], RouteVerifier.check([way_point], 0)
  end

  def test_check_reports_way_points
    way_points = [WayPoint.new(51.49871493, -0.1601177991, 1326378718),
                  WayPoint.new(51.49840586, -0.1604068824, 1326378723)]

    assert_equal [1], RouteVerifier.check(way_points, 7.5)
  end

  def test_check_reports_all_way_points_until_reasonable_speed_reached
    way_points = [WayPoint.new(51.49871493, -0.1601177991, 1326378718),
                  WayPoint.new(51.49840586, -0.1604068824, 1326378721),
                  WayPoint.new(51.49840586, -0.1604068824, 1326378722),
                  WayPoint.new(51.49840586, -0.1604068824, 1326378730)]

    assert_equal [1,2], RouteVerifier.check(way_points, 5)
  end

  def test_check_compares_way_points_with_last_reasonable_point
    way_points = [WayPoint.new(51.49815136, -0.1537985061, 1326379265),
                  WayPoint.new(51.5113867, -0.1756095886, 1326379271),
                  WayPoint.new(51.49944104, -0.1547229289, 1326379285),
                  WayPoint.new(51.49985835,	-0.1541135872,	1326379291)]

    assert_equal [1], RouteVerifier.check(way_points, 40)
  end
end
