require 'time'

class GpxExporter
  class << self
    def serialize_way_point(way_point)
      formatted_time = Time.at(way_point.timestamp).utc.iso8601
      "<trkpt lat=\"#{way_point.geo.latitude}\" lon=\"#{way_point.geo.longitude}\">"\
      "<time>#{formatted_time}</time></trkpt>"
    end

    def serialize(way_points)
      xml = '<?xml version="1.0" encoding="UTF-8"?>'
      xml += '<gpx version="1.1"><trk><trkseg>'

      way_points.each do |way_point|
        xml << serialize_way_point(way_point)
      end

      xml += '</trkseg></trk></gpx>'
    end

    def export(file_path, way_points)
      xml = serialize(way_points)

      File.open(file_path, 'w') do |file|
        file.write(xml)
      end
    end
  end
end
