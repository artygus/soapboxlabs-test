require 'test/unit'
require './lib/way_point'
require './lib/geo_point'

class WayPointSpec < Test::Unit::TestCase
  def test_negative_timestamp
    assert_raise WayPoint::NegativeTimestamp do
      WayPoint.new(0, 0, -1)
    end
  end

  def test_valid_timestamp
    now = Time.now.to_i
    point = WayPoint.new(0, 0, now)
    assert_equal now, point.timestamp
  end

  def test_valid_timestamp_from_string
    now = Time.now.to_i
    point = WayPoint.new(0, 0, now.to_s)
    assert_equal now, point.timestamp
  end

  def test_invalid_timestamp_string_value_raises_exception
    assert_raise ArgumentError do
      WayPoint.new(0, 0, '1c')
    end
  end

  def test_valid_geo
    point = WayPoint.new(60, 30, 0)
    assert_equal GeoPoint.new(60, 30), point.geo
  end
end
